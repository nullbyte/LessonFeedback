using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

using Surveys;

namespace TeacherClient
{
	public partial class QuestionResultItem : UserControl
	{
		public TextAnswerResponse response = new();
		
		public QuestionResultItem()
		{
			InitializeComponent();
		}

		public QuestionResultItem(TextAnswerResponse response)
		{
			InitializeComponent();

			this.response = response;
			QuestionLabel.Text = response.Question;
			ResultLabel.Text = response.Response;
		}
	}
}