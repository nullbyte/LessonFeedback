using System;
using Avalonia;
using Avalonia.Media;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

using Surveys;

namespace TeacherClient
{
	public partial class SurveyItem : UserControl
	{
		public Survey survey = new();
		public Status status;
		
		public SurveyItem()
		{
			InitializeComponent();
		}

		public SurveyItem(Survey survey) {
			InitializeComponent();

			this.survey = survey;
			NameLabel.Text = survey.FeedbackQuestionsTitle;
		}

		public Action<SurveyItem>? OnStart;
		public Action<SurveyItem>? OnStop;
		public Action<SurveyItem>? OnEdit;
		public Action? OnResponse;
		public Action<SurveyItem>? OnDelete;

		void ToggleSurvey(object sender, RoutedEventArgs e) {
			if (status != Status.ACTIVE) {
				if (OnStart != null) {
					OnStart(this);
				}
			} else {
				if (OnStop != null)
					OnStop(this);
			}
		}

		void EditSurvey(object sender, RoutedEventArgs e) {
			if (OnEdit != null)
				OnEdit(this);
		}

		void SurveyResponses(object sender, RoutedEventArgs e) {
			if (OnResponse != null)
				OnResponse();
		}
		
		void PromptDeleteSurvey(object sender, RoutedEventArgs e) {
			if (OnDelete != null)
				OnDelete(this);
		}

		public void SetStatus(Status s) {
			status = s;
			if (s == Status.ACTIVE) {
				StatusLabel.Text = "Active";
				StatusLabel.Foreground = Brush.Parse("#00FF00");
				PlayIcon.IsVisible = false;
				StopIcon.IsVisible = true;
			} else if (s == Status.INACTIVE) {
				StatusLabel.Text = "Inactive";
				ResponseButton.IsVisible = false;
			} else if (s == Status.OFFLINE) {
				StatusLabel.Text = "Offline";
				PlayButton.IsVisible = false;
				ResponseButton.IsVisible = false;
			}
		}

		public enum Status {
			ACTIVE,
			INACTIVE,
			OFFLINE
		}
	}
}