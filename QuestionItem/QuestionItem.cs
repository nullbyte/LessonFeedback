using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

using Surveys;

namespace TeacherClient
{
    public partial class QuestionItem : UserControl
    {
        public Question question = new();
		
		public QuestionItem()
        {
            InitializeComponent();
        }

		public QuestionItem(Question question)
        {
            InitializeComponent();

			this.question = question;

			QuestionBox.Text = question.questionText;
			MandatoryBox.IsChecked = question.manditory;
        }

		public Action<QuestionItem>? OnDelete;
		
		void DeleteQuestion(object sender, RoutedEventArgs e) {
			if (OnDelete != null)
				OnDelete(this);
		}
    }
}