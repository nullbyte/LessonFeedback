using System;
using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Interactivity;
using Avalonia.Input;
using Avalonia.Collections;

using Backend;
using Surveys;

namespace TeacherClient
{
	public partial class MainWindow : Window
	{
		// Lists to hold data for all the apps list views
		private AvaloniaList<SurveyItem> surveyItems = new();
		private AvaloniaList<QuestionItem> questionItems = new();
		private AvaloniaList<ResultItem> responseItems = new();
		
		// Objects for managing data
		private static Settings settings = new("LessonFeedbackTC");
		private static SurveyManager surveyManager = new(settings);
		private static NetworkManager networkManager = new();

		// The survey being edited
		Survey? activeSurvey;
		// If the app is online
		bool online = true;
		// The name of the currently published survey
		string? publishedSurvey;

		public MainWindow() {
			InitializeComponent();
			
			// Bind the lists to the list views
			FeedbackQuizList.Items = surveyItems;
			QuestionsList.Items = questionItems;
			ResultList.Items = responseItems;

			// Load data from disk and the web
			LoadSettings();
			UpdatePublishedSurvey();
			LoadSurveys();
		}

		void LoadSettings() {
			// Load application wide settings from disk
			NameLabel.Text = settings.Get("Name", "Guest");
		}

		async void UpdatePublishedSurvey() {
			// Check if the app is online and store the currently published survey in a variable
			online = await networkManager.IsOnline();
			if (online)
				publishedSurvey = (await networkManager.GetCurrentSurvey())?.FeedbackQuestionsTitle;
			LoadSurveys();
		}

		void LoadSurveys() {
			// Load surveys from disk into the main list
			surveyItems.Clear();

			foreach (var item in surveyManager.LoadAllSurveys()) {
				SurveyItem newSurvey = new(item);
				newSurvey.OnEdit = EditSurvey;
				newSurvey.OnStart = StartSurvey;
				newSurvey.OnStop = StopSurvey;
				newSurvey.OnResponse = ShowResponses;
				newSurvey.OnDelete = DeleteSurvey;

				// Set survey status depending on server status
				if (!online) {
					newSurvey.SetStatus(SurveyItem.Status.OFFLINE);
					surveyItems.Add(newSurvey);
				}
				else if (publishedSurvey == item.FeedbackQuestionsTitle) {
					newSurvey.SetStatus(SurveyItem.Status.ACTIVE);
					surveyItems.Insert(0, newSurvey);
				}
				else {
					newSurvey.SetStatus(SurveyItem.Status.INACTIVE);
					surveyItems.Add(newSurvey);
				}
			}
		}

		void ReloadSurveys(object sender, RoutedEventArgs e) {
			// When the reload button is pressed refresh the app
			UpdatePublishedSurvey();
			LoadSurveys();
		}
		
		void AddSurvey(object sender, RoutedEventArgs e) {
			EditSurvey(new SurveyItem());
		}

		void EditSurvey(SurveyItem item) {
			activeSurvey = item.survey;
			EditSurveyName.Text = activeSurvey.FeedbackQuestionsTitle;
			questionItems.Clear();

			foreach (var question in activeSurvey.TextAnswerQuestionList) {
				QuestionItem newQuestion = new(question);
				newQuestion.OnDelete = DeleteQuestion;
				questionItems.Add(newQuestion);
			}

			EmptySurveyLabel.IsVisible = questionItems.Count == 0;
			EditPanel.IsVisible = true;
		}

		async void ShowResponses() {
			// Get and show Responses from the internet
			if (online) {
				responseItems.Clear();
				ResultPanel.IsVisible = true;
				Responses? responses = await networkManager.GetResponses();
				
				if (responses != null) {

					foreach (var response in responses.responses) {
						ResultItem resultItem = new(response);
						resultItem.OnDelete = DeleteResponse;
						responseItems.Add(resultItem);
					}

				} else {
					ResultPanel.IsVisible = false;
					await new Dialog("Error", "Something went wrong while fetching the results, please try again later.", false).Show(AppView);
				}
			}
			EmptyResponseLabel.IsVisible = responseItems.Count == 0;
		}

		async void DeleteResponse(ResultItem item) {
			if (await new Dialog("Warning", "Are you sure you want to delete this response? this cannot be undone").Show(AppView)) {
				responseItems.Remove(item);

				Responses? responses = await networkManager.GetResponses();
				if (responses != null) {
					foreach (var i in responses.responses) {
						responses.responses.Remove(i);
						break;
					}
					await networkManager.UploadResponses(responses);
				} else {
					await new Dialog("Error", "Something went wrong while deleting the response, please try again later.", false).Show(AppView);
					responseItems.Add(item);
				}
			}
		}
		
		async void StartSurvey(SurveyItem item) {
			UpdatePublishedSurvey();
			if (!String.IsNullOrEmpty(publishedSurvey)) {
				if (!(await new Dialog("Activate survey?", "This will deactivate and clear responses from the currently active survey").Show(AppView))) {
					return;
				}
			}
			// Upload survey to the server
			item.survey.TeacherName = settings.Get("Name");
			await networkManager.ClearResponses();
			await networkManager.UploadSurvey(item.survey);
			publishedSurvey = item.survey.FeedbackQuestionsTitle;
			LoadSurveys();
		}

		async void StopSurvey(SurveyItem item) {
			// Clear survey from the server and remove responses
			if (await new Dialog("Warning", "This will deactivate the survey and clear all current responses").Show(AppView)) {
				await networkManager.ClearCurrentSurvey();
				publishedSurvey = null;
				LoadSurveys();
			}
		}
		
		async void DeleteSurvey(SurveyItem item) {
			// Delete survey from disk
			bool delete = await new Dialog("Delete?", $"Are you sure you want to delete the survey \"{item.survey.FeedbackQuestionsTitle}\"?").Show(AppView);
			if (delete) {
				surveyItems.Remove(item);
				surveyManager.DeleteSurvey(item.survey);
				if (publishedSurvey == item.survey.FeedbackQuestionsTitle) {
					await networkManager.ClearCurrentSurvey();
				}
			}
		}
		
		void ClosePanel(object sender, PointerPressedEventArgs e) {
			Rectangle rec = (Rectangle)sender;
			IControl? overlay = rec.Parent;
			if (overlay != null) {
				overlay.IsVisible = false;
			}
		}

		void ShowSettings(object sender, RoutedEventArgs e) {
			EditNameBox.Text = settings.Get("Name");
			SettingsPanel.IsVisible = true;
		}

		void SaveSettings(object sender, RoutedEventArgs e) {
			if (EditNameBox.Text == "" || EditNameBox.Text == null)
				settings.Remove("Name");
			else
				settings.Set("Name", EditNameBox.Text);
			LoadSettings();
			SettingsPanel.IsVisible = false;
		}

		void CloseSettings(object sender, RoutedEventArgs e) {
			SettingsPanel.IsVisible = false;
		}

		async void SaveSurvey(object sender, RoutedEventArgs e) {
			if (surveyManager.ListAllSurveys().Contains(EditSurveyName.Text??"") && !(EditSurveyName.Text == activeSurvey?.FeedbackQuestionsTitle)) {
				// Trying to create a survey with the same name as an existing one will overwrite it, so don't allow that
				await new Dialog("Invalid name", "A survey with this name already exists", false).Show(AppView);
			} else if (String.IsNullOrWhiteSpace(EditSurveyName.Text)) {
				// Saving/uploading a survey requires it has a name
				await new Dialog("Invalid name", "Please enter a survey name", false).Show(AppView);
			} else {
				// Delete the survey that was just edited, if it exists
				if (activeSurvey != null) {
					surveyManager.DeleteSurvey(activeSurvey);
					activeSurvey.TextAnswerQuestionList.Clear();
				} else {
					activeSurvey = new Survey();
				}
				
				activeSurvey.FeedbackQuestionsTitle = EditSurveyName.Text;

				// Add all questions to survey
				foreach (var item in questionItems) {
					if (item.QuestionBox.Text != null && item.QuestionBox.Text != "") {
						item.question.questionText = item.QuestionBox.Text;
						item.question.manditory = item.MandatoryBox.IsChecked??true;
						activeSurvey.TextAnswerQuestionList.Add(item.question);
					}
				}

				surveyManager.SaveSurvey(activeSurvey);

				EditPanel.IsVisible = false;
				// Reload list
				
				if (publishedSurvey == activeSurvey.FeedbackQuestionsTitle) {
					if (await new Dialog("Reupload?", "Would you like to reupload the new version of this survey?").Show(AppView)) {
						if (await new Dialog("Clear responses", "Would you like to clear the currently saved responses?").Show(AppView)) {
							await networkManager.ClearResponses();
						}
						activeSurvey.TeacherName = settings.Get("Name");
						await networkManager.UploadSurvey(activeSurvey);
					}
				}

				LoadSurveys();
			}
		}
		
		void AddQuestion(object sender, RoutedEventArgs e) {
			QuestionItem newQuestion = new();
			newQuestion.OnDelete = DeleteQuestion;
			questionItems.Add(newQuestion);
			EmptySurveyLabel.IsVisible = questionItems.Count == 0;
		}

		void DeleteQuestion(QuestionItem item) {
			questionItems.Remove(item);
			EmptySurveyLabel.IsVisible = questionItems.Count == 0;
		}

		void CloseSurvey(object sender, RoutedEventArgs e) {
			EditPanel.IsVisible = false;
		}
	}
}