using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Collections;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

using Surveys;

namespace TeacherClient
{
	public partial class ResultItem : UserControl
	{
		public Response response = new();
		AvaloniaList<QuestionResultItem> questionResultItems = new();
		
		public ResultItem()
		{
			InitializeComponent();
		}

		public ResultItem(Response response)
		{
			InitializeComponent();

			this.response = response;
			StudentNameBox.Text = response.StudentName;

			QuestionResultItemsList.Items = questionResultItems;

			foreach (var item in response.QuestionsAndAnswers)
			{
				AddQuestion(item);
			}
		}

		void AddQuestion(TextAnswerResponse answer) {
			QuestionResultItem newQuestionResult = new(answer);

			questionResultItems.Add(newQuestionResult);
		}

		public Action<ResultItem>? OnDelete;

		void DeleteResponse(object sender, RoutedEventArgs e) {
			if (OnDelete != null)
				OnDelete(this);
		}
	}
}