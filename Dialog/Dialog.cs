using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Input;

namespace TeacherClient
{
    public partial class Dialog : UserControl
    {		
		private TaskCompletionSource<bool> completedTask = new();

		public Dialog()
        {
            InitializeComponent();
        }

		public Dialog(string title, string message, bool cancelVisible = true) {
			InitializeComponent();
			
			TitleLabel.Text = title;
			DescriptionLabel.Text = message;
			ButtonCancel.IsVisible = cancelVisible;
		}

		public async Task<bool> Show(Panel parent) {
			parent.Children.Add(this);
			await completedTask.Task;
			parent.Children.Remove(this);
			return completedTask.Task.Result;
		}

		void OkClicked(object sender, RoutedEventArgs e) {
			completedTask.SetResult(true);
		}

		void CancelClicked(object sender, RoutedEventArgs e) {
			completedTask.SetResult(false);
		}
		void Close(object sender, PointerPressedEventArgs e) {
			completedTask.SetResult(false);
		}
    }
}