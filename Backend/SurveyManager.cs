using System;
using System.IO;
using System.Text.Json;
using System.Collections.Generic;

using Surveys;

namespace Backend;

class SurveyManager {
	private string savedir = "";
	
	public SurveyManager(Settings settings) {
		savedir = Path.Join(settings.configdirpath, "surveys");

		if (!Directory.Exists(savedir))
			Directory.CreateDirectory(savedir);
	}

	public void DeleteSurvey(Survey survey) {
		string filepath = Path.Join(savedir, survey.FeedbackQuestionsTitle + ".survey");
		File.Delete(filepath);
	}

	public void SaveSurvey(Survey survey) {
		string filepath = Path.Join(savedir, survey.FeedbackQuestionsTitle + ".survey");
		File.WriteAllText(filepath, SurveyConverters.SurveyToString(survey));
	}

	public Survey? LoadSurvey(string filename) {
		string data = File.ReadAllText(filename);
		return SurveyConverters.StringToSurvey(data);
	}

	public List<Survey> LoadAllSurveys() {
		List<Survey> result = new();
		foreach (var file in Directory.GetFiles(savedir)) {
			Survey? item = LoadSurvey(file);
			if (item != null)
				result.Add(item);
		}
		return result;
	}

	public List<string> ListAllSurveys() {
		List<string> result = new();
		foreach (var file in Directory.GetFiles(savedir)) {
			result.Add(Path.GetFileNameWithoutExtension(file));
		}
		return result;
	}
}

static class SurveyConverters {

	private static JsonSerializerOptions opts = new() {IncludeFields = true};

	static public Survey? StringToSurvey(string data) {
		Survey? result = new();
		try {
			result = JsonSerializer.Deserialize<Survey>(data, opts);
		} catch (JsonException) {
			return null;
		}
		return result;
	}

	static public Response? StringToResponse(string data) {
		Response? result = new();
		try {
			result = JsonSerializer.Deserialize<Response>(data, opts);
		} catch (JsonException) {
			return null;
		}
		return result;
	}

	static public Responses? StringToResponseList(string data) {
		Responses? result = new();
		try {
			result = JsonSerializer.Deserialize<Responses>(data, opts);
		} catch (JsonException) {
			return null;
		}
		return result;
	}

	static public string SurveyToString(Survey survey) {
		string result = JsonSerializer.Serialize<Survey>(survey, opts);
		return result;
	}

	static public string ResponseToString(Response response) {
		string result = JsonSerializer.Serialize<Response>(response, opts);
		return result;
	}

	static public string ResponsesToString(Responses responses) {
		string result = JsonSerializer.Serialize<Responses>(responses, opts);
		return result;
	}
}