﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Surveys;

namespace Backend;
public class NetworkManager
{
	// Getting and setting data requires different clients
	HttpClient getClient = new();
	HttpClient putClient = new();
	
	public NetworkManager() {
		// Set default headers for getting and setting data
		getClient.DefaultRequestHeaders.Add("X-Master-Key", Keys.MASTER);
		putClient.DefaultRequestHeaders.Add("X-Master-Key", Keys.MASTER);
		getClient.DefaultRequestHeaders.Add("X-Bin-Meta", "false");
	}

	public async Task<Survey?> GetCurrentSurvey() {
		// Get current survey from server
		string result = "";
		try {
			result = await getClient.GetStringAsync(Urls.SURVEYGET);
		} catch (HttpRequestException) {}
		return SurveyConverters.StringToSurvey(result);
	}
	
	public async Task<Responses?> GetResponses() {
		// Get responses from server
		string result = await getClient.GetStringAsync(Urls.RESPONSEGET);
		return SurveyConverters.StringToResponseList(result);
	}

	public async Task<bool> IsOnline() {
		// Check if it can access the server
		try {
			await getClient.GetAsync(Urls.SURVEYGET);
		} catch (HttpRequestException) {
			return false;
		}
		return true;
	}

	public async Task ClearCurrentSurvey() {
		// Create and upload empty survey and response
		Survey emptys = new();
		StringContent requests = new StringContent(SurveyConverters.SurveyToString(emptys));
		requests.Headers.ContentType = new MediaTypeHeaderValue("application/json");
		await putClient.PutAsync(Urls.SURVEYSET, requests);

		Responses emptyr = new();
		StringContent requestr = new StringContent(SurveyConverters.ResponsesToString(emptyr));
		requestr.Headers.ContentType = new MediaTypeHeaderValue("application/json");
		var result = await putClient.PutAsync(Urls.RESPONSESET, requestr);
	}

	public async Task ClearResponses() {
		// Create and upload empty responses
		Responses empty = new();
		StringContent request = new StringContent(SurveyConverters.ResponsesToString(empty));
		request.Headers.ContentType = new MediaTypeHeaderValue("application/json");
		var result = await putClient.PutAsync(Urls.RESPONSESET, request);
	}

	public async Task UploadSurvey(Survey survey) {
		// Convert survey to JSON and upload
		StringContent request = new StringContent(SurveyConverters.SurveyToString(survey));
		request.Headers.ContentType = new MediaTypeHeaderValue("application/json");
		await putClient.PutAsync(Urls.SURVEYSET, request);
	}

	public async Task UploadResponses(Responses responses) {
		// Convert responses to JSON and upload
		StringContent request = new StringContent(SurveyConverters.ResponsesToString(responses));
		request.Headers.ContentType = new MediaTypeHeaderValue("application/json");
		await putClient.PutAsync(Urls.RESPONSESET, request);
	}
}