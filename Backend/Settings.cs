using System;
using System.IO;
using System.Collections.Generic;
using System.Text.Json;

namespace Backend;
class Settings {
	// name of directory and main config
	public string appname = "";
	private string baseconfdir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
	public string configdirpath = "";
	public string configfilepath = "";
	private Dictionary<string, string> configdata = new();
	
	public Settings(string appname) {
		this.appname = appname;
		
		// Path: /home/user/.config/appname
		configdirpath = Path.Join(baseconfdir, appname);
		// Path: /home/user/.config/appname/appname.settings
		configfilepath = Path.Join(baseconfdir, Path.Join(appname, appname + ".settings"));
		
		// Create file/directory if it doesn't exist
		if (!File.Exists(configfilepath)) {
			Directory.CreateDirectory(configdirpath);
			// Dispose file object to ensure it doesn't throw an error when being read
			File.Create(configfilepath).Dispose();
		}

		// If file contains valid json, load it into a dictionary, otherwise use empty dictionary.
		try {
			configdata = JsonSerializer.Deserialize<Dictionary<string, string>>(File.ReadAllText(configfilepath))??new Dictionary<string, string> {};
		} catch (JsonException) {}
	}

	public string Get(string key, string defaultvalue = "") {
		// If a value for key has been set return it, otherwise return default
		if (configdata.ContainsKey(key)) {
			return configdata[key];
		} else {
			return defaultvalue;
		}
	}

	public void Set(string key, string value) {
		// Set dictionary key to value specified
		configdata[key] = value;
		
		// Update file
		string configstring = JsonSerializer.Serialize(configdata);
		File.WriteAllText(configfilepath, configstring);
	}

	public void Remove(string key) {
		// Remove key from dictionary
		configdata.Remove(key);
		
		// Update file
		string configstring = JsonSerializer.Serialize(configdata);
		File.WriteAllText(configfilepath, configstring);
	}

	public void Reset() {
		// Delete config directory
		Directory.Delete(configdirpath, true);
	}
}