using System;
using System.Collections.Generic;

namespace Surveys;

// Survey class
public class Survey {
	public string TeacherName = "";
	public string FeedbackQuestionsTitle = "";

	public List<Question> TextAnswerQuestionList = new();
}

// Individual question data
public class Question {
	public bool manditory = false;
	public string questionText = "";
}

// Container class for responses
public class Responses {
	public List<Response> responses = new();
}

// Response data
public class Response {
	public string StudentName = "";
	public DateOnly TimeSubmitted;

	public List<TextAnswerResponse> QuestionsAndAnswers = new();

	public override bool Equals(object? obj)
	{
		if (obj == null || !(obj is Response)) {
			return false;
		} else if (obj is Response) {
			if (StudentName == ((Response)obj).StudentName && TimeSubmitted == ((Response)obj).TimeSubmitted) {
				return true;
			}
		}

		return base.Equals(obj);
	}

	public override int GetHashCode()
	{
		return base.GetHashCode();
	}
}

// Individual question response data
public class TextAnswerResponse {
	public string Question = "";
	public string Response = "";
}