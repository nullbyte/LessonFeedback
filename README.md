# Lesson Feedback Teacher Client

**Setup**
To run the application make sure you have the latest version of dotnet 7 installed. To run the app, open the project directory in the terminal (CMD on windows) and execute `dotnet run`. To build a release executable run `dotnet publish -r [platform] -C Release` where `[platform]` is one of either `linux-x64` or `win-x64` or any other valid dotnet platform runtime (only linux and windows have been tested).

**Connectivity**
To connect to the remote server, Save the [Access.cs.stub](Backend/Access.cs.stub) file in the `Backend` directory as `Access.cs` and provide the URLs for connecting, as well as the master access key.
Note: The URLs for getting and setting data are different. For JSONBin the getting URL is the same as the setting URL but with `/latest` appended.
